import Vue from 'vue'
import Router from 'vue-router'
import InitiateFailure from '@/pages/InitiateFailure'
import InitiateAccount from '@/pages/InitiateAccount'
import MobileCodeVerification from '@/pages/MobileCodeVerification'
import MerchantType from '@/pages/MerchantType'
import IndividualForm from '@/pages/IndividualForm'
import CompanyForm from '@/pages/CompanyForm'
import ConnectToBank from '@/pages/ConnectToBank'
import BankForm from '@/pages/BankForm'
import Checks from '@/pages/Checks'
import AccountApproved from '@/pages/AccountApproved'
import AccountNotApproved from '@/pages/AccountNotApproved'
import store from '../store';

Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'InitiateFailure',
      component: InitiateFailure
    }, 
    {
      path: '/:platformName/:merchantId',
      name: 'InitiateAccount',
      component: InitiateAccount
    }, 
    {
      path: '/:platformName/:merchantId/code-verification',
      name: 'MobileCodeVerification',
      component: MobileCodeVerification
    }, 
    {
      path: '/:platformName/:merchantId/account-type',
      name: 'MerchantType',
      component: MerchantType
    },
    {
      path: '/:platformName/:merchantId/account-individual',
      name: 'IndividualForm',
      component: IndividualForm
    },
    {
      path: '/:platformName/:merchantId/account-company',
      name: 'CompanyForm',
      component: CompanyForm
    },
    {
      path: '/:platformName/:merchantId/connect-bank',
      name: 'ConnectToBank',
      component: ConnectToBank
    },
    {
      path: '/:platformName/:merchantId/select-bank-account',
      name: 'BankForm',
      component: BankForm
    },
    {
      path: '/:platformName/:merchantId/running-checks',
      name: 'Checks',
      component: Checks
    },
    {
      path: '/:platformName/:merchantId/account-approved',
      name: 'AccountApproved',
      component: AccountApproved,
      meta: { requiresAuth: true }
    },
    {
      path: '/:platformName/:merchantId/account-for-review',
      name: 'AccountNotApproved',
      component: AccountNotApproved
    } 
  ]
})

router.beforeEach((to, from, next) => {
    var flowCompleted = store.state.flowCompleted;

    //if merchant id is changed, the redirect to first page
    if(store.state.merchantId != to.params.merchantId 
      && to.name !==  'InitiateAccount' 
      &&  to.name !==  'InitiateFailure') {
      next({ path: '/biteheist/'+to.params.merchantId })
    } 
    else if (flowCompleted.merchantId ==  to.params.merchantId && 
              flowCompleted.status  && to.name !==  'AccountApproved'
              && to.name !== 'AccountNotApproved'
              ) {
      let path =  store.state.flowStatus == 'AccountApproved' ? 'account-approved' : 'account-for-review'
      next({ path: '/biteheist/'+flowCompleted.merchantId+'/'+path })
    }
    else if (flowCompleted.merchantId !=  to.params.merchantId && flowCompleted.status  && to.name !==  'AccountApproved' && to.name !== 'AccountNotApproved') {
      store.state.flowCompleted.status = false;
      next({ path: '/biteheist/'+to.params.merchantId })
    }
    else {
      console.log('came here 4 ');
      next()
    }
})

export default router
